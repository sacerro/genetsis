<?php

    use Illuminate\Database\Seeder;

    class DatabaseSeeder extends Seeder {
        /**
         * Seed the application's database.
         *
         * @return void
         */
        public function run () {
            $this->call(\Database\Seeders\ProvinciasSeeder::class);
            $this->call(\Database\Seeders\PreferenciasSeeder::class);
            $this->call(\Database\Seeders\UserGenderSeeder::class);
        }
    }
