<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFormDataTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_form_data', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable(false);
            $table->string('lastName', 255)->nullable(false);
            $table->text('address')->nullable(); // It's better to create 5 columns to separate Address
            $table->string('email', 255)->unique();
            $table->bigInteger('province')->unsigned();
            $table->foreign('province')->references('id_provincia')->on('provincias'); // We suppose PROVINCES will be never deleted
            $table->bigInteger('gender')->unsigned(); // Laravel loves BIGINTEGER for primary keys...
            $table->foreign('gender')->references('id')->on('user_genders'); // We suppose GENERES will be never deleted... Ok, I know what I write...
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user_form_data');
    }
}
