<?php

namespace Database\Seeders;

use App\Models\UserGender;
use Illuminate\Database\Seeder;

class UserGenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // I'm sorry
        $genres = [
            'Hombre',
            'Mujer',
            'Transgénero',
            'Intergénero',
            'Neutro',
            'Otros'
        ];
        foreach ($genres as $name) {
            $genre = new UserGender();
            $genre->name = $name;
            $genre->save();
        }
    }
}
