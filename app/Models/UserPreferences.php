<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPreferences extends Model {
    public $table = 'user_preferences';
    public $timestamps = false;

    public function userPreference() {
        return $this->belongsTo(Preferencia::class, 'user_preference');
    }
    public function userFormData() {
        return $this->belongsTo(UserFormData::class, 'user_form' );
    }
}
