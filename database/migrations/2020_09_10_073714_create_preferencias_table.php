<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('preferencias', function (Blueprint $table) {
            // Table doesn't have primary column, this could generate some problems
            $table->smallInteger('preferencia_id')->unsigned()->primary();
            $table->string('preferencia', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('preferencias');
    }
}
