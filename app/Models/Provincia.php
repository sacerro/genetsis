<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model {
    public $timestamps = false;
    public $primaryKey = 'id_provincia';
    protected $fillable = [
        'id_provincia',
        'provincia'
    ];
    public function users() {
        return $this->belongsToMany(UserFormData::class, 'id');
    }
}
