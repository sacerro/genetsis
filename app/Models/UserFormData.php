<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFormData extends Model {
    protected $fillable = [
        'name',
        'lastName',
        'address',
        'province',
        'gender'
    ];

    public function userPreferences() {
        return $this->hasMany(UserPreferences::class, 'user_form');
    }
    public function userProvince() {
        return $this->belongsTo(Provincia::class, 'province');
    }
    public function userGender() {
        return $this->belongsTo(UserGender::class, 'gender'); // hasOne...
    }
}
