<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_preferences', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_form')->unsigned();
            $table->foreign('user_form')->references('id')->on('user_form_data')->cascadeOnDelete();
            $table->smallInteger('user_preference')->unsigned();
            $table->foreign('user_preference')->references('preferencia_id')->on('preferencias')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user_preferences');
    }
}
