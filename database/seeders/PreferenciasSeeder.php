<?php

namespace Database\Seeders;

use App\Models\Preferencia;
use Illuminate\Database\Seeder;

class PreferenciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $preferences = [
            [1, 'Deportes'],
            [2, 'Cine'],
            [3, 'Motor'],
            [4, 'Informática'],
            [5, 'Cocina'],
            [6, 'Viajes'],
        ];
        foreach ($preferences as $preferenceRAW) {
            $preference = new Preferencia();
            $preference->preferencia_id = $preferenceRAW[0];
            $preference->preferencia = $preferenceRAW[1];
            $preference->save();
        }
    }
}
