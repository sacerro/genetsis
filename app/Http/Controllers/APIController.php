<?php

namespace App\Http\Controllers;

use App\Models\UserFormData;
use App\Models\UserGender;
use App\Models\Preferencia;
use App\Models\Provincia;
use App\Models\UserPreferences;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class APIController extends Controller {
    public function getConfig(): array {
        try {
            return [
                'status' => true,
                'data' => [
                    'genders' => UserGender::all()->toArray(),
                    'preferences' => Preferencia::all()->toArray(),
                    'provinces' => Provincia::all()->toArray()
                ]
            ];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => false,
                'error' => 'Servicio no disponible temporalmente. Por favor, inténtelo de nuevo más tarde.'
            ];
        }
    }
    public function addUser(Request $request): array {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-zA-ZÀ-ÿ\u00f1\u00d1\s\-]{3,}$/',
            'lastName' => 'required|regex:/^[a-zA-ZÀ-ÿ\u00f1\u00d1\s\-]{3,}$/',
            'address' => 'nullable',
            'email' => 'required|email|unique:App\Models\UserFormData,email',
            'province' => 'required|exists:App\Models\Provincia,id_provincia',
            'gender' => 'required|exists:App\Models\UserGender,id',
            'preferences' => 'nullable|array|exists:App\Models\Preferencia,preferencia_id',
        ], [
            'name.required' => 'El campo nombre es obligatorio.',
            'name.regex' => 'El campo nombre es inválido, por favor, compruebe el campo.',
            'lastName.required' => 'El campo apellidos es obligatorio.',
            'lastName.regex' => 'El campo apellidos es inválido, por favor, compruebe el campo.',
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El email introducido no es válido.',
            'email.unique' => 'Lo sentimos, solo se permite un registro por email.',
            'province.required' => 'El campo provincia es obligatorio.',
            'province.exists' => 'Por favor, seleccione una provincia del listado.',
            'gender.required' => 'El campo sexo es obligatorio.',
            'gender.exists' => 'Por favor, seleccione un sexo del listado.',
            'preferences.array' => 'Por favor, seleccione uno o más preferencias. Si no desea ninguna, desmarque las casillas.',
            'preferences.exists' => 'Por favor, seleccione (si lo desea) las preferencias del listado.',
        ]);
        if ($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->errors()
            ];
        }
        try {
            $userFormData = new UserFormData();
            $userFormData->name = $request->name;
            $userFormData->lastName = $request->lastName;
            $userFormData->address = $request->address;
            $userFormData->email = $request->email;
            $userFormData->userProvince()->associate(Provincia::find($request->province));
            $userFormData->userGender()->associate(UserGender::find($request->gender));
            $userFormData->save();
            foreach ($request->preferences as $preference) {
                $userPreferences = new UserPreferences();
                $userPreferences->userPreference()->associate(Preferencia::find($preference));
                $userPreferences->userFormData()->associate($userFormData);
                $userPreferences->save();
            }

            return [
                'status' => true,
                'message' => '¡Muchas gracias! Se ha registrado correctamente su formulario.'
            ];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => false,
                'message' => 'Servicio no disponible temporalmente, por favor, inténtelo de nuevo más tarde.'
            ];
        }
    }

    public function getUsers(): array {
        try {
            $users = UserFormData::with(['userPreferences', 'userProvince', 'userGender'])->get();
            $output = [];
            foreach ($users as $user) {
                $currentUserPreferences = [];
                foreach ($user->userPreferences as $userPreference) {
                    $currentUserPreferences[] = $userPreference->userPreference->preferencia;
                }
                $outputUser = [
                    'nombre' => $user->name,
                    'apellidos' => $user->lastName,
                    'dirección' => $user->address,
                    'email' => $user->email,
                    'provincia' => $user->userProvince->provincia,
                    'sexo' => $user->userGender->name,
                    'preferencias' => $currentUserPreferences
                ];
                $output[] = $outputUser;
            }
            return [
                'status' => true,
                'users' => $output
            ];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return [
                'status' => false,
                'message' => 'Servicio no disponible temporalmente, por favor, inténtelo de nuevo más tarde.'
            ];
        }
    }
}
