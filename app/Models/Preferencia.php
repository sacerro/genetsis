<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Preferencia extends Model {
    public $timestamps = false;
    public $primaryKey = 'preferencia_id';
    protected $fillable = [
        'preferencia_id',
        'preferencia'
    ];
}
